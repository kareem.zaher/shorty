class CreateShortCodes < ActiveRecord::Migration[5.1]
  def change
    create_table(:short_codes) do |t|
      t.string :original_url
      t.string :target_url
      t.timestamps
    end
    add_index :short_codes, :original_url
    add_index :short_codes, :target_url, unique: true
  end
end
