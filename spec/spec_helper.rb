require 'rack/test'
require 'airborne'

ENV['RACK_ENV'] = 'test'

require 'simplecov'

SimpleCov.start

require File.expand_path('../../config/environment', __FILE__)

ActiveRecord::Base.logger = nil

Airborne.configure do |config|
  config.rack_app =  API::Base
end

RSpec.configure do |config|
  config.mock_with :rspec
  config.expect_with :rspec
  config.raise_errors_for_deprecations!
  config.order = 'random'
  config.before(:suite) do
    DatabaseCleaner.strategy = :transaction
    DatabaseCleaner.clean_with(:truncation)
  end

  config.before(:each) do
    DatabaseCleaner.start
  end

  config.after(:each) do
    DatabaseCleaner.clean
  end

  config.include FactoryBot::Syntax::Methods
  config.before(:suite) do
    FactoryBot.find_definitions
  end
end
