# frozen_string_literal: true

require 'spec_helper'

describe Serializers::ShortCode::StatsSerializer do
  subject { serialization.serializable_hash[:short_code] }
  let(:short_code) { create(:short_code) }
  let(:serializer) { described_class.new(short_code) }
  let(:serialization) { ActiveModelSerializers::Adapter.create(serializer, adapter: :json) }

  it 'should have the default attributes' do
    expect(subject).to have_key(:startDate)
    expect(subject).to have_key(:redirectCount)
  end

  context 'If short_code has redirects' do
    before { create(:redirect, short_code_id: short_code.id) }
    it 'should have lastSeenDate' do
      expect(subject).to have_key(:lastSeenDate)
    end
  end
end
