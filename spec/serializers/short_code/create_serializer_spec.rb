# frozen_string_literal: true

require 'spec_helper'

describe Serializers::ShortCode::CreateSerializer do
  subject { serialization.serializable_hash[:short_code] }
  let(:short_code) { create(:short_code) }
  let(:serializer) { described_class.new(short_code) }
  let(:serialization) { ActiveModelSerializers::Adapter.create(serializer, adapter: :json) }

  it 'should have the correct attributes' do
    expect(subject).to have_key(:shortcode)
  end
end
