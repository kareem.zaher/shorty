# frozen_string_literal: true

require 'spec_helper'

describe ShortCode do
  subject { described_class.new(original_url: original_url, target_url: target_url) }

  context 'When correct attributes are provided' do
    let(:original_url) { 'http://kareem.com' }
    let(:target_url) { 'kareem' }

    it 'will be valid to save' do
      expect(subject.save).to eql(true)
    end
  end

  context 'When original_url or target url are nil' do
    let(:original_url) { nil }
    let(:target_url) { nil }

    it 'will be invalid to save' do
      expect(subject.save).to eql(false)
    end
  end

  context 'When target url' do
    let(:original_url) { 'http://kareem.com' }
    context 'has invalid chars' do
      let(:target_url) { 'karee%' }

      it 'will be invalid to save' do
        expect(subject.save).to eql(false)
      end
    end

    context 'has invalid length' do
      let(:target_url) { 'kareeme' }

      it 'will be invalid to save' do
        expect(subject.save).to eql(false)
      end
    end

    context 'already exists' do
      let(:target_url) { 'kareem' }
      before { described_class.create(original_url: original_url, target_url: target_url) }
      it 'will be invalid to save' do
        expect(subject.save).to eql(false)
      end
    end

    context 'already exists but with different case' do
      let(:target_url) { 'kareem' }
      let(:original_url) { 'http://www.googleeee.com' }
      it 'will be valid to save' do
        expect(subject.save).to eql(true)
      end
    end
  end
end
