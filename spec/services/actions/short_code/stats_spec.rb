# frozen_string_literal: true

require 'spec_helper'

describe Actions::ShortCode::Stats do
  subject { described_class.perform(params) }

  let(:params) do
    {
      shortcode: target_url
    }
  end

  context 'when target_url does not exist' do
    let(:target_url) { 'kareem' }
    it 'should raise 404 NotFound' do
      expect { subject }.to raise_error(Shorty::Exceptions::NotFound)
    end
  end

  context 'when all inputs are valid' do
    let(:target_url) { 'kareem' }

    before { create(:short_code, target_url: 'kareem') }

    it 'should return status 200' do
      expect(subject.response_code).to eq(200)
      expect(subject.response_body).not_to eq(nil)
    end
  end
end
