# frozen_string_literal: true

require 'spec_helper'

describe Actions::ShortCode::Create do
  subject { described_class.perform(params) }

  let(:params) do
    {
      original_url: original_url,
      target_url: target_url
    }
  end

  context 'when original_url is empty' do
    let(:original_url) { nil }
    let(:target_url) { 'example' }

    it 'should raise 400 Bad Request exception' do
      expect { subject }.to raise_error(Shorty::Exceptions::BadRequest)
    end
  end

  context 'when target_url is empty' do
    let(:original_url) { Faker::Internet.url }
    let(:target_url) { nil }

    context 'and system was able to generate a new one' do
      it 'saves the randomly generated string' do
        expect(subject.response_body).not_to eq(nil)
        expect(subject.response_body[:shortcode]).not_to eq(nil)
      end
    end

    context 'and system was not able to generate a new one' do
      before do
        create(:short_code, target_url: 'static')
        allow(SecureRandom).to receive(:hex).and_return('static')
      end

      it 'should raise 500 Internal error' do
        expect { subject }.to raise_error(Shorty::Exceptions::InternalError)
      end
    end
  end

  context 'when target_url already exists' do
    let(:original_url) { Faker::Internet.url }
    let(:target_url) { 'kareem' }

    before { create(:short_code, target_url: 'kareem') }
    it 'should raise 409 Conflict exception' do
      expect { subject }.to raise_error(Shorty::Exceptions::Conflict)
    end
  end

  context 'when target_url is invalid' do
    let(:original_url) { Faker::Internet.url }
    let(:target_url) { Faker::Base.regexify(/^[0-9a-zA-Z_]{7}$/) }

    it 'should raise 409 Conflict exception' do
      expect { subject }.to raise_error(Shorty::Exceptions::UnprocessableEntity)
    end
  end

  context 'when all inputs are valid' do
    let(:original_url) { Faker::Internet.url }
    let(:target_url) { Faker::Base.regexify(/^[0-9a-zA-Z_]{6}$/) }

    it 'should return status 201 and correct output' do
      expect(subject.response_code).to eq(201)
      expect(subject.response_body).not_to eq(nil)
      expect(subject.response_body).to have_key(:shortcode)
      expect(subject.response_body[:shortcode]).to eq(target_url)
    end
  end
end
