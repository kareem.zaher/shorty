# frozen_string_literal: true

module API
  module V1
    module ErrorHandler
      def self.included(target_klass)
        target_klass.class_eval do
          rescue_from Shorty::Exceptions::BadRequest do |e|
            error!(e, 400)
          end
          rescue_from Shorty::Exceptions::Conflict do |e|
            error!(e, 409)
          end
          rescue_from Shorty::Exceptions::UnprocessableEntity do |e|
            error!(e, 422)
          end
          rescue_from Shorty::Exceptions::NotFound do |e|
            error!(e, 404)
          end
          rescue_from Shorty::Exceptions::InternalError do |e|
            error!(e, 500)
          end
        end
      end
    end
  end
end
