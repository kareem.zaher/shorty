# frozen_string_literal: true

module Shorty
  module Exceptions
    class BadRequest           < StandardError; end
    class Conflict             < StandardError; end
    class UnprocessableEntity  < StandardError; end
    class NotFound             < StandardError; end
    class InternalError        < StandardError; end
  end
end
