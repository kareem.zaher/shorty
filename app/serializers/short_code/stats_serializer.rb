# frozen_string_literal: true

require 'models/short_code'

module Serializers
  module ShortCode
    class StatsSerializer < ActiveModel::Serializer
      attributes :startDate, :redirectCount
      attribute  :lastSeenDate, if: :redirects?

      def redirects?
        object.redirects.present?
      end

      def startDate
        object.created_at.iso8601
      end

      def redirectCount
        object.redirects.count
      end

      def lastSeenDate
        object.redirects.last.created_at.iso8601
      end
    end
  end
end
