# frozen_string_literal: true

# Successful Redirects Model
class Redirect < ActiveRecord::Base
  belongs_to :short_code
end
