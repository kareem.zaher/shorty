# frozen_string_literal: true

require 'services/actions/base'
require 'models/short_code'
require 'serializers/short_code/create_serializer'

module Actions
  module ShortCode
    class Create < Actions::Base
      ErrorMessages = {
        bad_request: 'url is not present',
        invalid_short_code: 'The shortcode fails to meet the following regexp: ^[0-9a-zA-Z_]{6}$.',
        conflict_short_code: 'The the desired shortcode is already in use. Shortcodes are case-sensitive.',
        failed_to_generate_short_code: 'Failed to generate a random short code.'
      }.freeze

      def initialize(params)
        @original_url = params[:original_url]
        @target_url   = params[:target_url] || generate_random_target_url
        super
      end

      def perform
        return respond_with_failure(400, ErrorMessages[:bad_request]) if @original_url.nil?
        return respond_with_failure(422, ErrorMessages[:invalid_short_code]) unless target_url_valid?
        return respond_with_failure(409, ErrorMessages[:conflict_short_code]) if target_url_exists?
        initialize_short_code
        return respond_with_failure unless @short_code.save
        respond_with_success(serialize_output, 201)
      end

      private

      def initialize_short_code
        @short_code = ::ShortCode.new(original_url: @original_url, target_url: @target_url)
      end

      def serialize_output
        ActiveModelSerializers::SerializableResource.new(
          @short_code,
          serializer: ::Serializers::ShortCode::CreateSerializer,
          adapter: :json
        ).serializable_hash[:short_code]
      end

      def generate_random_target_url
        10.times do
          SecureRandom.hex(3).tap do |token|
            return token unless ::ShortCode.exists?(target_url: token)
          end
        end
        # unlikely to happen but better safe than sorry
        respond_with_failure(500, ErrorMessages[:failed_to_generate_short_code])
      end

      def target_url_exists?
        ::ShortCode.where(target_url: @target_url).present?
      end

      def target_url_valid?
        /^[0-9a-zA-Z_]{6}$/.match?(@target_url)
      end
    end
  end
end
